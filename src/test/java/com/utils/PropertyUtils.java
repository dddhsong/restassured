package com.utils;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class PropertyUtils {

    public static Properties propertyLoader(String filaPath) {
        Properties properties = new Properties();
        BufferedReader reader;

        try {
            reader = new BufferedReader(new FileReader(filaPath));
            try {
                properties.load(reader);
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException("failed to load properties file" + filaPath);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("properties file not found at "+ filaPath);
        }
        return properties;

    }



}
