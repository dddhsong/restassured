package com.spotfy.oauth2.tests.tests;

import com.api.StatusCode;
import com.api.applicationApi.PlaylistApi;
import com.spotify.oauth2.pojo.Error;
import com.spotify.oauth2.pojo.Playlist;
import com.utils.DataLoader;
import io.qameta.allure.*;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static com.utils.FakerUtils.generateDescription;
import static com.utils.FakerUtils.generateName;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;


@Epic("Spotify Oauth 2.0")
@Feature("Playlist API")
public class PlaylistTests extends BaseTest {

    @Step
    public Playlist playlistbuilder(String name, String description, boolean _public) {
        return Playlist.builder().
                name(name).
                description(description).
                _public(_public).
                build();

    }

    @Story("Create a playlist story")
    @Link("https://example.org")
    @Link(name = "allure", type = "mylink")
    @TmsLink("12345")
    @Issue("1234567")
    @Description("this is the description")
    @Test(description = "should be abel to create a playlist")
    public void ShouldBeAbleToCreateAPlaylist() {
        Playlist requestPlaylist = playlistbuilder(generateName(), generateDescription(),false );

        Response response = PlaylistApi.post(requestPlaylist);
        assertStatusCode(response.statusCode(), StatusCode.CODE_201.getCode());

        Playlist responsePlaylist = response.as(Playlist.class);
        assertPlaylistEqual(response.as(Playlist.class), requestPlaylist);
    }

    @Step
    private void assertStatusCode(int statusCode, int expectedStatusCode) {
        assertThat(statusCode, equalTo(expectedStatusCode));
    }

    @Step
    private void assertPlaylistEqual(Playlist responsePlaylist, Playlist requestPlaylist) {

        assertThat(responsePlaylist.getName(), equalTo(requestPlaylist.getName()));
        assertThat(responsePlaylist.getDescription(), equalTo(requestPlaylist.getDescription()));
        assertThat(responsePlaylist.get_public(), equalTo(requestPlaylist.get_public()));
    }

    @Test
    public void ShouldBeAbleToGetAPlaylist() {

        Playlist requestPlaylist = playlistbuilder("Updated Playlist Name", "Updated playlist description",false );

        Response response = PlaylistApi.get(DataLoader.getInstance().getGetPlaylistId());
        assertStatusCode(response.statusCode(), StatusCode.CODE_201.getCode());

        Playlist responsePlaylist = response.as(Playlist.class);
        assertPlaylistEqual(response.as(Playlist.class), requestPlaylist);

    }


    @Test
    public void ShouldBeAbleToUpdateAPlaylist() {
        Playlist requestPlaylist = playlistbuilder(generateName(), generateDescription(),false );


        Response response = PlaylistApi.update(DataLoader.getInstance().getUpdatePlaylistId(), requestPlaylist);
        assertStatusCode(response.statusCode(), StatusCode.CODE_200.getCode());

        Playlist responsePlaylist = response.as(Playlist.class);
        assertPlaylistEqual(response.as(Playlist.class), requestPlaylist);


    }
    @Story("Create a playlist story")
    @Test
    public void ShouldNotBeAbleToCreateAPlaylistWithName() {
        Playlist requestPlaylist = playlistbuilder("", generateDescription(),false );

        Response response = PlaylistApi.post(requestPlaylist);
        assertStatusCode(response.statusCode(), StatusCode.CODE_400.getCode());

        Playlist responsePlaylist = response.as(Playlist.class);


        Error error = response.as(Error.class);

        assertError(response.as(Error.class), requestPlaylist);

        assertThat(error.getError().getStatus(), equalTo(StatusCode.CODE_400.getCode()));
        assertThat(error.getError().getMessage(), equalTo(StatusCode.CODE_400.getMsg()));


    }

    private void assertError(Error error, Playlist requestPlaylist) {
        assertThat(error.getError().getStatus(), equalTo(StatusCode.CODE_400.getCode()));
        assertThat(error.getError().getMessage(), equalTo(StatusCode.CODE_400.getMsg()));

    }
    @Story("Create a playlist story")
    @Test
    public void ShouldNotBeAbleToCreateAPlaylistWithExpiredtoken() {
        String invalid_token = "1234";

        Playlist requestPlaylist = playlistbuilder(generateName(), generateDescription(),false );

        Response response = PlaylistApi.post(invalid_token,requestPlaylist);

        assertThat(response.statusCode(),equalTo(StatusCode.CODE_401.getCode()));

        Error error = response.as(Error.class);

        assertThat(error.getError().getStatus(), equalTo(StatusCode.CODE_401.getCode()));
        assertThat(error.getError().getMessage(), equalTo(StatusCode.CODE_401.getMsg()));

    }

}
