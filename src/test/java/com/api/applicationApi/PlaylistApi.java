package com.api.applicationApi;

import com.api.Restresource;
import com.spotify.oauth2.pojo.Playlist;
import com.utils.ConfigLoader;
import io.qameta.allure.Step;
import io.restassured.response.Response;

import static com.api.Route.PLAYLISTS;
import static com.api.Route.USERS;
import static com.api.SpecBuilder.getRequestSpec;
import static com.api.SpecBuilder.getResponseSpec;
import static com.api.TokenManager.getToken;

import static io.restassured.RestAssured.given;

public class PlaylistApi {
    //static String access_token = "BQDGbKOyg-b1GPPWLhOK4RGi8EgLVU8bHCyNn0g_ZUkERg3pzezO1Hz5YF3Q13_ztyn0j-gvV1R-XUoIWQRhT2Lw-ViOn-MlbqUr4qRnOS5olf-ehAx3DIUBAgAQY-SV9CApeF3AjVXrM1dNFj2pH2jpzqGUUCiAduC3p4o-w_o01QgpS4Ts5HuoqB5XnrJcUT7AVClaNThalAPqeU_5yJLMZl-9sZ5Pyz-MwlLRIJq4";

    @Step
    public static Response post(Playlist requestPlaylist) {

        return Restresource.post(USERS + "/"+ ConfigLoader.getInstance().getUser() + PLAYLISTS ,getToken(),requestPlaylist);

    }
    public static Response post(String token, Playlist requestPlaylist) {
        return Restresource.post(USERS + "/" + ConfigLoader.getInstance().getUser() + PLAYLISTS,token,requestPlaylist);

    }

    public static Response get(String playlistId) {
        return Restresource.get(PLAYLISTS + "/"  + playlistId,getToken());

    }

    public static Response update(String playlistId, Playlist requestPlaylist) {
        return Restresource.update(PLAYLISTS + "/"  + playlistId,getToken(),requestPlaylist);
    }



}
