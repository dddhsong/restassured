package com.api;

import com.utils.ConfigLoader;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.time.Instant;
import java.util.HashMap;

import static com.api.SpecBuilder.getResponseSpec;
import static io.restassured.RestAssured.given;
import static io.restassured.filter.log.LogDetail.*;

public class TokenManager {

    static String access_token;
    static Instant expiry_time;

    public synchronized static String getToken() {

        try {
            if (access_token == null || Instant.now().isAfter(expiry_time)) {
                Response response = renewToken();
                access_token = response.path("access_token");
                int expiryDurationInSeconds = response.path("expires_in");
                expiry_time = Instant.now().plusSeconds(expiryDurationInSeconds - 300);
            } else {
                System.out.println("Token is good to use");
            }


        } catch (Exception e) {
            throw new RuntimeException("Abort!! Failed to get token");
        }

        return access_token;
    }


    private static Response renewToken() {

        String access_token = "BQA8n0_uWADPh1qsepNyXe8yCrtD40sR3zwQrOCpgcOC9hvrhWVvaJ7M6F9aevwt6OQ2Dxq2_adQ0eFEnsZIx8lDSIawMccVAuXjvjkWARvluXJ8S0M016xnC-9zX2XqbS6LWymRxKPdrFFu8dUnfjhMPyDQmPuWvFlasjdTer4OrZdd66l4Vi3SISU2rtBQX229R_nQp-She5SRscxThO5SLMnOpq4bqybGKgpIWj3K";
        String refresh_token = "AQDdfeVa2SIGE9qNtym6ZFaV-eqvkHz0htp4FqjpqEs8bsBmf2fNxefl8vcRvlsDCBJOGwa-ichKoqRuwkwQytUMg6wG4I4AU8BDsIFhQtxy4MFm1L-1OVHWLN4EUEMupOM";

        HashMap<String, String> formParams = new HashMap<String, String>();
        formParams.put("client_id", ConfigLoader.getInstance().getClientId());
        formParams.put("client_secret",ConfigLoader.getInstance().getClientSecret());
        formParams.put("refresh_token",ConfigLoader.getInstance().getRefreshToken());
        formParams.put("grant_type",ConfigLoader.getInstance().getGrantType());

        Response response = Restresource.postAccount(formParams);

        if (response.statusCode() !=200) {
            throw new RuntimeException("ABORT!!! Renew Token failed");

        }

        return response;
    }
}
